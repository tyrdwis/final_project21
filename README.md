# Final Project
## Kelompok 21
<hr>

## Anggota Kelompok
- Fitria Dewi Wulandari
- Tiara Dwi Syaputri
<hr>

## Tema Project
Website e-Learning untuk pengajar
<hr>

## ERD
<img src="ERD.png">
<hr>

## Link Video
###### Link Demo Aplikasi: https://youtu.be/rC-fCGjYsXw
###### Link Template : https://technext.github.io/material_able/
###### Link Deploy(Optional) : https://agile-plateau-04445.herokuapp.com/ 
