<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Material;
use App\Course;

class MaterialController extends Controller
{
    public function create(){
        $subject = new Course;
        return view('material.create', compact('subject'));
    }

    public function store(Request $request){
        $request -> validate([
            'type' => 'required',
            'title' => 'required',
            'desc' => 'required'
        ]);
        //dd($request);
        
        $material = new Material;
        $material->type = $request["type"];
        $material->title = $request["title"];
        $material->desc = $request["desc"];
        $material->exlink = $request["exlink"];
        $material->deadline = $request["deadline"];
        $material->subject_id = $request["subject_id"];
        
        $material->save();
        
        return redirect('/material')->with('success', 'Success!');
    }

    public function edit($id){
        $material = Material::find($id);
        $subject = Course::find($id);
        //$material = DB::table('materials')->where('id', $id)->first();
        //$subject = DB::table('subjects')->where('id', $id)->first();
        //dd($material);
        return view('material.edit', compact('material','subject'));
    }

    public function index(){
        return view('material.index');
    }

    public function show($id){
        $material = Material::find($id);
        $subject = Course::find($id);
        //$material = DB::table('materials')->where('id', $id)->first();
        //$subject = DB::table('subjects')->where('id', $id)->first();
        //dd($material);
        return view('material.show', compact('material'));
    }

    public function update($id, Request $request){
        $request->validate([
            'type' => 'required',
            'title' => 'required',
            'desc' => 'required'
        ]);
        //dd($request);

        $material = Material::find($id);

        $material->type = $request["type"];
        $material->title = $request["title"];
        $material->desc = $request["desc"];
        $material->exlink = $request["exlink"];
        $material->deadline = $request["deadline"];
        $material->subject_id = $request["subject_id"];
        
        $material->update();
        
        return redirect('/material')->with('success', 'Berhasil Update Subject!');
    }

    public function destroy($id){
        $material = DB::table('materials')->where('id', $id)->delete();
        
        return redirect('/material')->with('success', 'Success!');
    }
}
