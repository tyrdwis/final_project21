<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function show(){
        $user = Auth::user();
        //dd($user);
        return view('profile.store', compact('user'));
    }
    public function store(Request $request){
        $request -> validate([
            'tanggal_lahir' => 'required',
            'alamat' => 'required'
        ]);
        //dd($request);
        
        $profile = new Profile;
        $profile->tanggal_lahir = $request["tanggal_lahir"];
        $profile->alamat = $request["alamat"];
        $profile->user_id = $request["user_id"];
        //dd($profile);
        $profile->save();

        return redirect('/user-profile')->with('success', 'Success!');
    }


    public function index(){
        $user = Auth::user();
        //dd($user);
       
        return view('profile.show', compact('user'));
    }


}

