<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Course;
use App\Material;
use Auth;

class CourseController extends Controller
{

    public function create(){
        return view('subject.create');
    }

    public function store(Request $request){
        $request -> validate([
            'judul' => 'required',
            'desc_materi' => 'required'
        ]);

        $subject = new Course;
        $subject->judul = $request["judul"];
        $subject->desc_materi = $request["desc_materi"];
        $subject->user_id = Auth::id();
        $subject->save();

        return redirect('/subject')->with('success', 'Success!');
    }

    public function index(){
        $subjects = DB::table('subjects')->get();
        // $subjects = Course::with(['id'])->get();
        return view('subject.index', compact('subjects'));
    }

    public function show($id){
        $subjects = Course::find($id);
        //$materials = Material::find($id);
        $materials = Material::all();
        //dd($materials);
        return view('subject.show', compact('subjects','materials'));
    }

    public function destroy($id){
        $subjects = DB::table('subjects')->where('id', $id)->delete();
        
        return redirect('/subject')->with('success', 'Success!');
    }

    public function edit($id){
        $subjects = DB::table('subjects')->where('id', $id)->first();
        // dd($subject);
        return view('subject.edit', compact('subjects'));
    }

    public function update($id, Request $request){
        $request -> validate([
            'judul' => 'required',
            'desc_materi' => 'required'
        ]);

        $update = Course::where('id', $id)->update([
            "judul" => $request["judul"],
            "desc_materi" => $request["desc_materi"]
        ]);

        return redirect('/material')->with('success', 'Updated!');
    }
}
