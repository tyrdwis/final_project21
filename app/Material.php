<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = "materials";
    protected $fillable = ['type','title','desc','exlink','deadline','subject_id'];
    public function course(){
        return $this->belongsTo('App\Course','subject_id');
    }
}
