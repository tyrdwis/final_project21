<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "subjects";
    
    public function material(){
        return $this->hasMany("App\Material", "subject_id");
    }

    
}
