<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/subject','CourseController@index')->middleware('auth');
Route::get('/subject/create','CourseController@create')->middleware('auth');
Route::post('/subject','CourseController@store')->middleware('auth');
Route::get('/subject/{id}','CourseController@show')->middleware('auth');
Route::delete('/subject/{id}', 'CourseController@destroy')->middleware('auth');
Route::get('/subject/{id}/edit','CourseController@edit')->middleware('auth');
Route::put('/subject/{id}', 'CourseController@update')->middleware('auth');

Route::get('/material/create','MaterialController@create')->middleware('auth');
Route::post('/subject/{id}', 'MaterialController@store')->middleware('auth');
Route::get('/material/{id}','MaterialController@show')->middleware('auth');
Route::delete('/material/{id}', 'MaterialController@destroy')->middleware('auth');
Route::get('/material', 'MaterialController@index')->middleware('auth');

Route::get('/profile','ProfileController@show')->middleware('auth');
Route::post('/profile','ProfileController@store')->middleware('auth');
Route::get('/user-profile','ProfileController@index')->middleware('auth');
Route::put('/user-profile','ProfileController@update')->middleware('auth');

Route::get('/material/{id}/edit','MaterialController@edit')->middleware('auth');
Route::put('/material/{id}','MaterialController@update')->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
