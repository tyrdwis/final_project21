@extends("layouts.master")

@section("content")
<div>
<div class="card">
    <div class="card-header">
        <h5>Sistem Akuntansi dan Manajemen Keuangan</h5>
        <div class="card-header-right">
            <ul>
                <li><i class="fa fa-minus minimize-card"></i></li>
            </ul>
        </div>
    </div>
        <div class="card-block table-border-style">
        @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Subject</th>
                            <th>Description</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($subjects as $key => $subject)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$subject->judul}}</td>
                                <td>{{$subject->desc_materi}}</td>
                                <td style="display: flex;">
                                    <a href="/subject/{{$subject->id}}" class="btn btn-primary btn-sm">Show</a>
                                    <a href="/subject/{{$subject->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                <form action="/subject/{{$subject->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                                </td>
                            </tr>
                        @empty
                            <tr align="center">
                                <td>No Subject</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <a href="http://127.0.0.1:8000/subject/create" class="btn btn-primary btn-sm">Add New Subject</a>
        </div>
    </div>
</div>
@endsection