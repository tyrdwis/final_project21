@extends("layouts.master")

@section("content")

<div>
<div class="card">
    <div class="card-header">
        <h4>{{$subjects->judul}}</h4>
        <h6>{{$subjects->desc_materi}}</h6>
        <div class="card-header-right">
            <ul>
                <li><i class="fa fa-minus minimize-card"></i></li>
            </ul>
        </div>
    </div>
    <div class="card-block table-border-style">
    @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
    @endif
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th style="width: 10px">No</th>
                        <th>Title</th>
                        <th>Deskripsi</th>
                        <th style="width: 250px">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($materials as $key=>$material)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$material->title}}</td>
                        <td>{{$material->desc}}</td>
                        <td  style="display: flex;">
                            <a href="/material/{{$material->id}}" class="btn btn-primary btn-sm">Show</a>
                            <a href="/material/{{$material->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/material/{{$material->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>No Subject</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <a class="btn btn-primary mb-2 ml-2 btn-sm" href="/material/create">Add New Material</a>
    </div>
</div>
</div>

@endsection