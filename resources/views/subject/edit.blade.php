@extends("layouts.master")

@section("content")
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Edit Subject</h5>
            </div>
            <div class="card-block">
                <form role="form" action="/subject/{{$subjects->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label for="judul" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input value="{{old('judul', $subjects->judul)}}" type="text" class="form-control" id="judul" name="judul" placeholder="Type subject title">
                            @error('judul')
                            <div class="alert alert-danger">The title field is required</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="desc_materi" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" class="form-control" id="desc_materi" name="desc_materi" placeholder="Type subject description">{{old('desc_materi', $subjects->desc_materi )}}</textarea>
                            @error('desc_materi')
                            <div class="alert alert-danger">The description field is required</div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
        
    </div>
</div>
@endsection