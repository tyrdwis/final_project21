@extends("layouts.master")

@section("content")
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>New Material</h5>
            </div>
            <div class="card-block">
                <form role="form" action="/subject/{id}" method="POST">
                @csrf
                <div class="form-group row">
                        <label for="type" class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-10">
                            <select name="type" id="type" class="form-control fill">
                                <option value="opt1">Select One Value Only</option>
                                <option value="opt2">Assignment</option>
                                <option value="opt3">Information</option>
                                <option value="opt4">Learning Materials</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input id="title" name="title" type="text" class="form-control" placeholder="Type your title">
                            @error('title')
                            <div class="alert alert-danger">The title field is required</div>
                            @enderror
                        </div>
                    </div>
                    <div for="desc" class="form-group row">
                        <label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="desc" name="desc" rows="5" cols="5" class="form-control" placeholder="Type description"></textarea>
                            @error('desc')
                            <div class="alert alert-danger">The description field is required</div>
                            @enderror
                        </div>
                    </div>
                    <div for="exlink" class="form-group row">
                        <label class="col-sm-2 col-form-label">External Link</label>
                        <div class="col-sm-10">
                            <input id="exlink" name="exlink" type="text" class="form-control" placeholder="Paste your external link">
                        </div>
                    </div>
                    <div for="deadline" class="form-group row">
                        <label class="col-sm-2 col-form-label">Deadline</label>
                        <div class="col-sm-10">
                            <input name="deadline" id="deadline" type="date" class="form-control">
                        </div>
                    </div>
                    <!-- <div for="upfile" class="form-group row">
                        <label class="col-sm-2 col-form-label">Link File</label>
                        <div class="col-sm-10">
                            <input id="upfile" name="upfile" type="text" class="form-control">
                        </div>
                    </div> -->
                        <div>
                            <form method="post" action="/subject/{{$subject->id}}">
                                @csrf
                                <button href="/subject/{{$subject->id}}" type="submit" class="btn btn-primary">Create</button>
                                <input type="hidden" value="{{Auth::user()->id}}" id="subject_id" name="subject_id">
                            </form>
                            <a href="/subject/{{$subject->id}}" class="btn btn-danger">Exit</a>
                        </div>

            </div>
        </div>
    </div>
</div>
@endsection
