@extends("layouts.master")

@section("content")

<div class="card">
    <div class="card-header">
        <h5>{{$material->title}}</h5>
        <div class="card-header-right">
            <ul>
                <li><i class="fa fa-minus minimize-card"></i></li>
            </ul>
        </div>
    </div>
    <div class="card-block table-border-style">
        <p>{{$material->desc}}</p>
        <p>Link eksternal yang bisa diakses : {{$material->exlink}}</p>
        <p>Deadline : {{$material->deadline}}</p>
    </div>

@endsection