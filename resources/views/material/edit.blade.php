@extends("layouts.master")

@section("content")
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>New Material</h5>
            </div>
            <div class="card-block">
                <form role="form" action="/material/{{$material->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group row">
                        <label for="type" class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-10">
                            <select name="type" id="type" value="{{ old('type', $material->type) }}" class="form-control fill">
                                <option value="opt1">Select One Value Only</option>
                                <option value="opt2">Assignment</option>
                                <option value="opt3">Information</option>
                                <option value="opt4">Learning Materials</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input id="title" name="title" type="text" class="form-control" value="{{ old('title', $material->title) }}" placeholder="Type your title">
                            @error('title')
                            <div class="alert alert-danger">The title field is required</div>
                            @enderror
                        </div>
                    </div>
                    <div for="desc" class="form-group row">
                        <label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="desc" name="desc" rows="5" cols="5" class="form-control" placeholder="Type description">{{ old('desc', $material->desc) }}</textarea>
                            @error('desc')
                            <div class="alert alert-danger">The description field is required</div>
                            @enderror
                        </div>
                    </div>
                    <div for="exlink" class="form-group row">
                        <label class="col-sm-2 col-form-label">External Link</label>
                        <div class="col-sm-10">
                            <input id="exlink" name="exlink" type="text" class="form-control" value="{{ old('exlink', $material->exlink) }}" placeholder="Paste your external link">
                        </div>
                    </div>
                    <div for="deadline" class="form-group row">
                        <label class="col-sm-2 col-form-label">Deadline</label>
                        <div class="col-sm-10">
                            <input name="deadline" id="deadline" type="date" class="form-control" value="{{ old('deadline', $material->deadline) }}">{{ old('deadline', $material->deadline) }}
                        </div>
                    </div>
                    
                    <div>
                        <form method="post" action="/material/{{$material->id}}">
                            @csrf
                            @method('PUT')
                            <button href="/material/{{$material->id}}" type="submit" class="btn btn-primary">Update</button>
                            <input type="hidden" value="1" id="subject_id" name="subject_id">
                        </form>
                    </div>

            </div>
        </div>
    </div>
</div>
@endsection