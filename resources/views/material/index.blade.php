@extends("layouts.master")

@section("content")

<div>
    <h3>Success!</h3>
    <a href="/subject" class="btn btn-danger">Back to dashboard</a>
</div>
@endsection