@extends("layouts.master")

@section("content")
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Profil</h5>
            </div>
            <div class="card-block">
                <form role="form" action="/profile" method="POST">
                @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="{{Auth::user()->name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="{{Auth::user()->email}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input id="tanggal_lahir" name="tanggal_lahir" type="date" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" id="alamat" name="alamat" class="form-control" placeholder="Masukkan Alamat"></textarea>
                        </div>
                    </div>
                    <div>
                        <form method="post" action="/profile">
                            @csrf
                            <button href="/profile" type="submit" class="btn btn-primary">Save</button>
                            <input type="hidden" value="{{$user->id}}" id="user_id" name="user_id">
                        </form>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection