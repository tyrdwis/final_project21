@extends("layouts.master")

@section("content")
<div class="row">
    <div class="col-sm-12">
        <!-- Basic Form Inputs card start -->
        <div class="card">
            <div class="card-header">
                <h5>Profil</h5>
            </div>
            <div class="card-block">
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="{{$user->name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="{{$user->email}}" readonly>
                        </div>
                    </div>
                    

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control" placeholder="{{$user->profile->tanggal_lahir}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" id="alamat" name="alamat" class="form-control" placeholder="Masukkan Alamat">{{$user->profile->alamat}}</textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection